import { Injectable } from '@angular/core';
import { DatosUsuario } from '../interfaces/tarjeta.interface';
import { AgregarTarjetaComponent } from '../components/agregar-tarjeta/agregar-tarjeta.component';
@Injectable({
  providedIn: 'root'
})
export class TarjetaService {
  [x: string]: any;

   listUsuarios: DatosUsuario[] = [
    {titular:'Scarleth Guzman', numeroTarjeta: '15633221122558855', fechaExpiracion: '02/20'},
    {titular:'Vidal guzman', numeroTarjeta: '15633221122558855', fechaExpiracion: '07/21'},
    {titular:'Miguel Valencia', numeroTarjeta: '15633221122558855', fechaExpiracion: '10/22'},
    {titular:'Daniela Rea', numeroTarjeta: '15633221122558855', fechaExpiracion: '05/25'},
  ];
  constructor() { }

  

  getUsuario(): DatosUsuario[] {
    return this.listUsuarios.slice()
  }

  eliminarUsuario(index:number){
    this.listUsuarios.splice(index,1);
  }

}

